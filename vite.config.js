import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    {
      name: "vite-plugin-style-import",
      handleHotUpdate({ file, server }) {
        if (file.endsWith(".scss")) {
          server.moduleGraph.invalidateModules(/\.vue/);
        }
      },
    },
  ],
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `@import '@/css/app.scss';`,
      },
    },
  },
  resolve: {
    alias: {
      "@": resolve(__dirname, "src"),
    },
  },
});

